//
//  FavAlbumDpo.m
//  Deezcovery
//
//  Created by B'n'J on 27/02/2015.
//  Copyright (c) 2015 B'n'J. All rights reserved.
//

#import "FavAlbumDpo.h"
#import "FavArtistDpo.h"


@implementation FavAlbumDpo

@dynamic id;
@dynamic title;
@dynamic cover;
@dynamic artist;

@end
