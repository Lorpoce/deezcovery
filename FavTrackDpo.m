//
//  FavTrackDpo.m
//  Deezcovery
//
//  Created by B'n'J on 27/02/2015.
//  Copyright (c) 2015 B'n'J. All rights reserved.
//

#import "FavTrackDpo.h"
#import "FavAlbumDpo.h"


@implementation FavTrackDpo

@dynamic id;
@dynamic title;
@dynamic track;
@dynamic album;

@end
