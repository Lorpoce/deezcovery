//
//  ArtistListViewController.h
//  Deezcovery
//

#import <UIKit/UIKit.h>

@interface ArtistListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@end