//
//  Artist.h
//  Deezcovery
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Artist : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *picture;
@property (strong, nonatomic) UIImage *UIpicture;
@property (strong, nonatomic) NSString *_id;
@end
